+++
title = "Wolf Family Cookbook"
description = ""
date = "2019-11-11"
lastmodifierdisplayname = "Alex Wolf"
+++

## Cook Book Sections

{{%children style="h3" %}}

## Contributing:

### Adding a recipe: 

Send Alex an email with either the recipie or a link to a recipie that you want added.

### Updating A recipie

If you see an issue with a recipie, or make it and make a teak that makes it better send Alex an email with the changes you want added

### Photos:

If you make a recipie and it looks good, send me a picture so I can add it to the site showing what it looks like.