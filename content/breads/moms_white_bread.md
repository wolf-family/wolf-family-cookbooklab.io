+++
title = "Adeline's White Bread"
date = "2019-11-10"
lastmodifierdisplayname = "Adeline McCarney"
tags= ["Adeline's"]
+++

## Steps:

| 3 Loaves | 1 Loaf | 6 Loaves |
| -------- | ------ | -------- |
|Dissolve | Dissolve | Dissolve |
|4 tbsp. Oil | 1 tbsp. Oil | 6 tbsp. Oil |
|½ Cup Sugar | 1/6 Cup Sugar | 1 Scant Cup Sugar |
|4 ½ tsp. Salt | 1 ½ tsp. Salt | 3 tbsp. Salt |
|Add | Add | Add |
|4 Cups of Flour | 1 1/3 Cups of Flour | 8 Cups of Flour |
|1 tbsp. yeast | 1 tsp. yeast | 2 tbsp. yeast |
|3 ¾ Cups Water | 1 ¼ Cups Water | 7 ½ Cups Water |
|4-5 Cups flour | 1 ½ Cups | 8-10 Cups |

* Place in a lightly oiled bowl and roll dough around.
* Let rise for 90 Minutes
* Punch down and let rise for another 60 minutes
* Transfer to pans and let rise for another 60 minutes
* Bake at 375

